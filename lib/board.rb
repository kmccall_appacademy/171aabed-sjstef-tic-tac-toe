class Board
    attr_reader :grid
    
    def initialize(grid=Array.new(9))
      @grid = grid
    end
    
    def to_s
        board = ""
        (0..8).each do |position|
            board << " #{@grid[position] || (position+1).to_s} "
            case position % 3
            when 0,1 then board << "|"
            when 2 then board <<  "\n-----------\n" unless position == 8
            end
        end
        board
    end
    
    def check_tile(tile)
      @grid[tile-1]
    end
    
    def place_mark(pos, mark)
        if self.empty?(pos) == true
            @grid[pos-1] = mark.to_sym
        else
            puts "Cannot place mark there, the tile is occupied."
            return move = false
        end
    end
    
    def empty?(pos)
        empty = false
        empty = true if @grid[pos-1] == nil
        empty
    end
    
    def winner(mark)
        winner = false
        winner = true if self.over?(mark) == true
        winner
    end
    
    def over?(mark)
         possibilities = {
            a: [@grid[0], @grid[1], @grid[2]],
            b: [@grid[3], @grid[4], @grid[5]],
            c: [@grid[6], @grid[7], @grid[8]],
            d: [@grid[0], @grid[3], @grid[6]],
            e: [@grid[1], @grid[4], @grid[7]],
            f: [@grid[2], @grid[5], @grid[8]],
            g: [@grid[0], @grid[4], @grid[8]],
            h: [@grid[2], @grid[4], @grid[6]]
         }
         
        over = false
        
        ('a'..'h').to_a.each do |option|
            matches = possibilities[option.to_sym].select{|tile| tile == mark.to_sym}
            over = true if matches.length == 3
            return over if over == true    
        end
        over
    end
    
    def tie(mark)
      tie = false
      unless @grid.include?(nil)
        if self.winner(mark) == false
          puts "The match is a tie!" 
          tie = true
        end
      end
      tie
    end
    
    
    def clear
      @grid.map!{|elem| elem = nil}
    end
    
end
