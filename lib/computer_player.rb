require_relative "human_player.rb"
require_relative "board.rb"

class ComputerPlayer < HumanPlayer
    attr_reader :mark, :pos
    
    def initialize(mark="O", board)
      @mark = mark
      @board = board
    end

   
    def defaults
      if @board.check_tile(5) == nil
        @board.place_mark(5, "O")
      else
        rand < 0.499 ? sides : corners
      end
    end
    
    def get_move
      if self.winning_move.any?
        @board.place_mark(self.winning_move.first, "O")
        puts @board
        
        self.final("O")
      elsif self.block_opponent.any?
        @board.place_mark(self.block_opponent.first, "O")
      else
        self.defaults
      end
    end    

    def sides
      [2, 4, 6, 8].each do |tile|
        return @board.place_mark(tile, @mark)  if @board.check_tile(tile) == nil
      end
    end
    
    def corners
      [1, 3, 7, 9].each do |tile|
        return @board.place_mark(tile, @mark) if @board.check_tile(tile) == nil
      end
    end

    Private
    
    def winning_move
      ftw = []
      possibilities = {
            a: [@board.check_tile(1), @board.check_tile(2), @board.check_tile(3)],
            b: [@board.check_tile(4), @board.check_tile(5), @board.check_tile(6)],
            c: [@board.check_tile(7), @board.check_tile(8), @board.check_tile(9)],
            d: [@board.check_tile(1), @board.check_tile(4), @board.check_tile(7)],
            e: [@board.check_tile(2), @board.check_tile(5), @board.check_tile(8)],
            f: [@board.check_tile(3), @board.check_tile(6), @board.check_tile(9)],
            g: [@board.check_tile(1), @board.check_tile(5), @board.check_tile(9)],
            h: [@board.check_tile(3), @board.check_tile(5), @board.check_tile(7)]
         }
      
      indexes = {
            a: [1, 2, 3],
            b: [4, 5, 6],
            c: [7, 8, 9],
            d: [1, 4, 7],
            e: [2, 5, 8],
            f: [3, 6, 9],
            g: [1, 5, 9],
            h: [3, 5, 7]
        }
      
      ('a'..'h').each do |option|
        if possibilities[option.to_sym].count(@mark.to_sym) == 2 && possibilities[option.to_sym].include?(nil)
          indx = possibilities[option.to_sym].each_index.select{|idx| possibilities[option.to_sym][idx] == nil}
          ftw << indexes[option.to_sym][indx.first]
        end
      end
      
      ftw
    end
    
    def block_opponent
      block =[]
      possibilities = {
            a: [@board.check_tile(1), @board.check_tile(2), @board.check_tile(3)],
            b: [@board.check_tile(4), @board.check_tile(5), @board.check_tile(6)],
            c: [@board.check_tile(7), @board.check_tile(8), @board.check_tile(9)],
            d: [@board.check_tile(1), @board.check_tile(4), @board.check_tile(7)],
            e: [@board.check_tile(2), @board.check_tile(5), @board.check_tile(8)],
            f: [@board.check_tile(3), @board.check_tile(6), @board.check_tile(9)],
            g: [@board.check_tile(1), @board.check_tile(5), @board.check_tile(9)],
            h: [@board.check_tile(3), @board.check_tile(5), @board.check_tile(7)]
         }
      
      indexes = {
            a: [1, 2, 3],
            b: [4, 5, 6],
            c: [7, 8, 9],
            d: [1, 4, 7],
            e: [2, 5, 8],
            f: [3, 6, 9],
            g: [1, 5, 9],
            h: [3, 5, 7]
      }
      
      ('a'..'h').each do |option|
       if possibilities[option.to_sym].count(:X) == 2 && possibilities[option.to_sym].include?(nil)
          indx = possibilities[option.to_sym].each_index.select{|idx| possibilities[option.to_sym][idx] == nil}
          block << indexes[option.to_sym][indx.first]
       end
      end
       
      block
       
    end
    
end
