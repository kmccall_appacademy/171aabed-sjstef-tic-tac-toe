require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
    attr_accessor :mark, :pos, :board
    
    def initialize
      @board = Board.new
      @player1 = HumanPlayer.new("X", @board)
      self.game_setup
    end
    
    def game_setup
      puts "\e[#{36}m#{"Welcome to Tic-Tac-Toes"}\e[0m".center(25) 
      puts "\nWould you like to versus the computer or another person? (Respond computer or human)"
      player_choice = gets.chomp.downcase
      exit! if player_choice == "exit"
      if player_choice == "human"
        @player2 = HumanPlayer.new("O", @board)
      elsif player_choice == "computer"
        @player2 = ComputerPlayer.new("O", @board)
      else
        puts "I didn't understand your response"
        self.game_setup
      end
      
      # puts "Would you like to go first?"
      # first_player = gets.chomp.downcase
      # if first_player == "yes"
      #   @player1.display
      #   puts "\nPlayer 1 turn: "
      #   @player1.get_move
      # elsif first_player == "no"
      #   @player2.display
      #   puts "\nPlayer 2 turn:"
      #   @player2.get_move
      # end
      
      self.switching_players
    end
      
    def switching_players
      until @board.over?(@player1.mark || @player2.mark)
        puts "\n"
        puts @board
        puts "\nPlayer 1 turn:"
        @player1.get_move
        if @board.over?(@player1.mark) != true
           puts "\nPlayer 2 turn:"
           @player2.get_move
        end
      end
    end
      
end
