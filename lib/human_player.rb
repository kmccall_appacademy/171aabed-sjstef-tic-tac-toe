

class HumanPlayer
    attr_reader :mark, :pos
    #require "board.rb"
    
    def initialize(mark="X", board)
        @mark = mark
        @board = board
    end
    
    def display
        puts @board
    end
    
    Private
    
    def get_move
        puts "Where would you like to make your move (tiles 1-9)?"
        move = gets.chomp.to_i
        @board.place_mark(move, @mark) 
        self.display
        self.final(@mark)
    end
    
    def final(mark)
      if @board.winner(@mark)
          puts "#{@mark} is the winner! Would you like to play again?"
          self.restart
      elsif @board.tie(@mark)
          puts "Would you like to play again?"
          self.restart
      end
    end
    
    def restart
        answer = gets.chomp.to_s.downcase
        if answer == "yes"
          @board.clear
          self.display
          self.get_move
        elsif answer == "no"
          puts "Thank you for playing!"
          exit!
        else
          puts "I didn't understand your response, would you like to play again?"
          answer = gets.chomp.to_s.downcase
        end
    end
    
end
